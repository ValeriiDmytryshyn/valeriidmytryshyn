﻿using System;

namespace Lab_4
{
    class Program
    {
        static void Main(string[] args)
        {
             Console.WriteLine("Enter length of array");
            if (int.TryParse(Console.ReadLine(), out int Length) && Length > 0)
            {
                Random random = new Random();
                int[] array = new int[Length];
                for (int i = 0; i < Length; i++){array[i] = random.Next(0, 100);}
                int[] cloneArray = (int[])array.Clone();
                Array.Sort(cloneArray);
                int min = Array.IndexOf(array, cloneArray[0]);
                int max = Array.IndexOf(array, cloneArray[Length-1]);
                for (int i = 0; i < array.Length; i++){Console.Write($" {array[i]} ");}
                if (min > max)
                {
                    int cloud = min;
                    min = max;
                    max = cloud;
                }
                
                    int[] innerArray;
                    if (max == 0) { innerArray = new int[min - 1]; }
                    else if (min == 0) {innerArray = new int[max - 1]; }
                    else { innerArray = new int[max - min - 1]; }
                    int counter = 0;
                    for (int i = min + 1; counter < innerArray.Length; i++)
                    {
                        innerArray[counter] = array[i];
                        counter++;
                    }
                    Array.Reverse(innerArray);
                    counter = min;
                    for (int i = 0; i < innerArray.Length; i++)
                    {
                        array[counter + 1] = innerArray[i];
                        counter++;
                    }
                Console.WriteLine($"\n Min index {min} \n Max index {max}");
                for (int i = 0; i < array.Length; i++){Console.Write($" {array[i]} ");}
            }
            else
            {
                Console.WriteLine("Incorrect input");
            }
        }
    }
}
