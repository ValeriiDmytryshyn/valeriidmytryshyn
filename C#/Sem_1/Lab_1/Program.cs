﻿using System;

namespace Lab_1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Write time in seconds");
            string sec = Console.ReadLine();

            int[] array = new int[3];
            if (int.TryParse(sec, out int Sec))
            {
                array[2] = Sec % 60;
                array[1] = Sec / 60 % 60;
                array[0] = Sec / 60 / 60;
                Console.WriteLine($"{array[0]} hours, {array[1]} minutes, {array[2]} seconds");
            }
            else
            {
                Console.WriteLine("Incorrect time enter");
            }
        }
    }
}
