﻿using Lab_9.Abstract;
namespace Lab_9.Entities
{
    static class Painter
    {
        public static void Draw(IDraw shape)
        {
            shape.Draw();
        }
    }
}
