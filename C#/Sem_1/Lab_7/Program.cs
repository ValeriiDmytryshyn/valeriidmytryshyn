﻿using System;
using System.Collections.Generic;

namespace Lab_7
{
    class Program
    {
        static void Main(string[] args)
        {
            List<string> strings = new List<string>();
            bool flag = true;
            while (flag)
            {
                Console.WriteLine("Enter string");
                strings.Add(Console.ReadLine());
                Console.WriteLine("Add more? (y/n)");
                if (Console.ReadLine() != "y")
                {
                    flag = false;
                }
            }
            foreach (var item in strings)
            {
                Console.WriteLine($" {item} ");
            }
            Console.WriteLine($"Length - {strings.Count}");
        }
    }
}
