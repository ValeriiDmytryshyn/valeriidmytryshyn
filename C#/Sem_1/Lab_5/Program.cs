﻿using System;

namespace Lab_5
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter m");
            string m = Console.ReadLine();
            Console.WriteLine("Enter n");
            string n = Console.ReadLine();
            Console.WriteLine("Your matrix\n");
            if (int.TryParse(m, out int row) && int.TryParse(n, out int coloumn))
            {
                int[,] array = new int[row, coloumn];
                Random ran = new Random();
                for (int i = 0; i < row; i++)
                {
                    for (int j = 0; j < coloumn; j++)
                    {
                        array[i, j] = ran.Next(1, 15);
                        Console.Write("{0}\t", array[i, j]);
                    }
                    Console.WriteLine();
                }
                int paired = 0;
                int unpaired = 0;
                for (int i = 0; i < row; i++)
                {
                    for (int j = 0; j < coloumn; j++)
                    {
                        if (i % 2 == 0){ paired += array[i, j]; }
                        else { unpaired += array[i, j]; }
                    }
                }
                Console.WriteLine($"\nSum of paired - {paired}");
                Console.WriteLine($"Sum of unpaired - {unpaired}");
            }
        }
    }
}
