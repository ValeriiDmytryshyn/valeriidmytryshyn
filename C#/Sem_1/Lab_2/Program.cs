﻿using System;

namespace Lab_2
{
    class Program
    {
        static void Main(string[] args)
        {
            while (true)
            {
                Console.WriteLine("Which task?( First/Second )");
                if (Console.ReadLine().ToLower() == "first")
                {
                    Console.WriteLine("Enter radius of ball");
                    string radius = Console.ReadLine();
                    if (float.TryParse(radius, out float R))
                    {
                        Console.WriteLine(4 * Math.PI * (R * R));
                    }
                    else
                    {
                        Console.WriteLine("Incorrect input");
                    }
                }
                else
                {
                        int k;
                        double sum;
                        Console.WriteLine("Enter nn");
                        int.TryParse(Console.ReadLine(), out int n1);
                        Console.WriteLine("Enter nk");
                        int.TryParse(Console.ReadLine(), out int n2);
                        sum = 0;
                        if ((n1 >= 0) && (n2 >= n1))
                        {
                            for (k = n1; k <= n2; k++)
                            {
                                sum += (k * k + MathF.Pow(-1, k - 1) * 2 * k - 1) / (k * k + 8);
                            }
                            Console.WriteLine(sum);
                        }
                        else
                        {
                            Console.WriteLine("Incorrect input");
                        }
                }
            }
        }
    }
}
