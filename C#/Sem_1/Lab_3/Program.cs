﻿using System;

namespace Lab_3
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] array = new int[3];
            Console.WriteLine("Enter first number");
            if (!int.TryParse(Console.ReadLine(), out array[0]))
            {
                Console.WriteLine("Incorrect input");
                return;
            }
            Console.WriteLine("Enter second number");
            if (!int.TryParse(Console.ReadLine(), out array[1]))
            {
                Console.WriteLine("Incorrect input");
                return;
            }
            Console.WriteLine("Enter third number");
            if (!int.TryParse(Console.ReadLine(), out array[2]))
            {
                Console.WriteLine("Incorrect input");
                return;
            }
            for (int i = 0; i < array.Length; i++)
            {
                for (int j = 0; j < array.Length; j++)
                {
                    if (array[i] == array[j] * -1)
                    {
                        Console.WriteLine("True");
                        return;
                    }
                }
            }
            Console.WriteLine("False");
        }
    }
}
