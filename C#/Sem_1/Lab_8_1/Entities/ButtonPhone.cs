﻿using System;
namespace Lab_8_2.Entities
{
    class ButtonPhone : DiskPhone
    {
        public bool Buttons = true;
        public override void Call()
        {
            Console.WriteLine("I'm calling from phone with buttons");
        }

    }
}
