﻿using System;

namespace Lab_8_2.Entities
{
    class ColorPhone : BlackWhitePhone
    {
        public bool wifi = true;
        public override void Call()
        {
            Console.WriteLine("I'm calling from phone with color display");
        }
        public override void RingTone()
        {
            Console.WriteLine("Normal sound");
        }
        public virtual void SurfTheNet()
        {
            Console.WriteLine(".NET _valeron4iko_");
        }

    }
}
