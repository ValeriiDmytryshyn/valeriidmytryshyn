﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lab_8_2.Entities
{
    class DiskPhone
    {
        public bool calls = true;
        public int numOfDigits = 10;

        public virtual void Call()
        {
            Console.WriteLine("I'm calling from disk phone");
        }

        public virtual void RingTone()
        {
            Console.WriteLine("diing don");
        }
    }
}
