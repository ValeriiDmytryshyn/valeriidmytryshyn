﻿using System;
namespace Lab_8_2.Entities
{
    class BlackWhitePhone : ButtonPhone
        {
            public bool display = true;
            public override void Call()
            {
                Console.WriteLine("I'm calling from phone with black and white display");
            }

            public override void RingTone()
            {
                Console.WriteLine("Dingggg donn");
            }
        }
    
}
