﻿using System;
namespace Lab_8_2.Entities
{
    class Smartphone : ColorPhone
    {
        public override void Call()
        {
            Console.WriteLine("Well, it the last call in your life)");
        }
        public override void RingTone()
        {
            Console.WriteLine("Something cool");
        }
        public override void SurfTheNet()
        {
            Console.WriteLine("Do you want to watch YouTube?");
        }
    }
}
