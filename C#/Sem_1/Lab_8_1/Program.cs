﻿using Lab_8_2.Entities;
using System;

namespace Lab_8_1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Disk Phone");
            DiskPhone dp = new DiskPhone();
            dp.Call();
            Console.WriteLine();

            Console.WriteLine("Button Phone");
            ButtonPhone bp = new ButtonPhone();
            bp.RingTone();
            Console.WriteLine();

            Console.WriteLine("Black&White Phone");
            BlackWhitePhone nokia = new BlackWhitePhone();
            nokia.Call();
            nokia.RingTone();
            Console.WriteLine();

            Console.WriteLine("Color Phone");
            ColorPhone colorPhone = new ColorPhone();
            colorPhone.Call();
            colorPhone.SurfTheNet();
            Console.WriteLine();

            Console.WriteLine("iPhone");
            Smartphone iphone = new Smartphone();
            iphone.Call();
            iphone.SurfTheNet();
            Console.WriteLine();
        }
    }
}
