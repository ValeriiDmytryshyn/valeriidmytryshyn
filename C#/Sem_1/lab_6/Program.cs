﻿using System;

namespace lab_6
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter value for reverse");
            string answer = Console.ReadLine();
            if (int.TryParse(answer, out int number))
            {
                Console.WriteLine(Reverse(number));
            }
            else if (double.TryParse(answer, out double numberD))
            {
                Console.WriteLine(Reverse(numberD));
            }
            else
            {
                Console.WriteLine(Reverse(answer));
            }
        }
        static string Reverse(int number) 
        {
            string strNumber = number.ToString();
            string outNumber = "";
            for (int i = strNumber.Length - 1; i >= 0; i--)
            {
                outNumber += strNumber[i];
            }
            return outNumber;
        }
        static string Reverse(string str)
        {
            string outString = "";
            if (str.Split(",").Length <= 1)
            {
                for (int i = str.Length - 1; i >= 0; i--)
                {
                    outString += str[i];
                }
                return outString;
            }
            else
            {
                string[] Str = str.Split(",");
                for (int i = Str[0].Length - 1; i >= 0; i--)
                {
                    outString += Str[0][i];
                }
                outString += ",";
                for (int i = Str[1].Length - 1; i >= 0; i--)
                {
                    outString += Str[1][i];
                }
                return outString;
            }
        }
        static string Reverse(double flo)
        {
            string[] str = flo.ToString().Split(",");
            string outNumber = "";
            for (int i = str[0].Length - 1; i >= 0; i--)
            {
                outNumber += str[0][i];
            }
            outNumber += ",";
            for (int i = str[1].Length - 1; i >= 0; i--)
            {
                outNumber += str[1][i];
            }
            return outNumber;
        }
    }
}
