﻿using System;
using System.Collections.Generic;
using System.Linq;
namespace Lab_8
{
    public class Garage
    {
        private List<Car> cars = new List<Car>();
        public void Create(Car car) 
        {
            cars.Add(car);
        }
        public List<Car> ListItems() { return cars; }
        private Car FindItemBtId(int id) 
        {
            if (cars.Any(x => x.Id == id))
            {
                return cars.First(x => x.Id == id);
            }
            throw new ArgumentException("No car with this id");
        }
        public void Delete(int carId)
        {
            cars.Remove(FindItemBtId(carId));
        }
        public int CountCars() { return cars.Count(); }
        public List<Car> FindItems(string carName)
        {
            if (cars.Any(x => x.CarName == carName))
            {
                return cars.Where
                    (x => x.CarName == carName).ToList();
            }
            throw new ArgumentException("No car with this parameters");
        }
        public List<Car> FindItems(char[] color)
        {
            if (cars.Any
                (x => x.Color == color.ToString()))
            {
                return cars.Where
                    (x => x.Color == color.ToString()).ToList();
            }
            throw new ArgumentException("No car with this parameters");
        }
        public List<Car> FindItems(int speed)
        {
            if (cars.Any
                (x => x.Speed == speed))
            {
                return cars.Where
                    (x => x.Speed == speed).ToList();
            }
            throw new ArgumentException("No car with this parameters");
        }
        public List<Car> FindItems(float year)
        {
            if (cars.Any
                (x => x.GraduationYear == year))
            {
                return cars.Where
                    (x => x.GraduationYear == year).ToList();
            }
            throw new ArgumentException("No car with this parameters");
        }
        public List<Car> FindItems(string carName, string color, int speed, int graduationYear) 
        {
            if (cars.Any
                (x => x.CarName == carName && x.Color == color && x.Speed == speed && x.GraduationYear == graduationYear))
            {
                return cars.Where
                    (x => x.CarName == carName && x.Color == color && x.Speed == speed && x.GraduationYear == graduationYear).ToList();
            }
            throw new ArgumentException("No car with this parameters");
        }
        public string Ride(int id) 
        {
            if (cars.Any(x => x.Id == id))
            {
                return $"\nBrooo you are ride on {FindItemBtId(id).CarName} with max speed in {FindItemBtId(id).Speed}\n";
            }
            else
            {
                throw new ArgumentException("No car with this id");
            }
        }
    }

}
