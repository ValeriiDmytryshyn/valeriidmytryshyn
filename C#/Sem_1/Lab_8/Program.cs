﻿using System;

namespace Lab_8
{
    class Program
    {
        static void Main(string[] args)
        {
            Garage ShechGarage = new Garage();
            while (true)
            {
                Console.WriteLine("what are you want to do? ( Add new car(add)/ Remove car(remove)/ Ride on the car(ride) )");
                string answer = Console.ReadLine();
                if (answer.ToLower() == "add")
                {
                    try
                    {

                        Car car = new Car();
                        Console.WriteLine("Enter car name");
                        string name = Console.ReadLine();
                        if (name.Length > 0) { car.CarName = name; }
                        Console.WriteLine("Enter car color");
                        string color = Console.ReadLine();
                        if (color.Length > 0) { car.Color = color; }
                        Console.WriteLine("Enter car speed");
                        string speed = Console.ReadLine();
                        if (int.TryParse(speed, out int Speed)) { car.Speed = Speed; }
                        Console.WriteLine("Enter car graduation year");
                        string graduationYear = Console.ReadLine();
                        car.Id = ShechGarage.CountCars()+1;
                        if (int.TryParse(graduationYear, out int GraduationYear)) { car.GraduationYear = GraduationYear; }
                        if (!string.IsNullOrEmpty(car.Color) && !string.IsNullOrEmpty(car.CarName) && car.GraduationYear != 0 && car.Speed != 0)
                        {
                            ShechGarage.Create(car);
                        }
                        else
                        {
                            Console.WriteLine("Incorrect enter");
                        }
                    }
                    catch (ArgumentException ex)
                    {
                        Console.WriteLine(ex.Message);
                    }
                }
                else if (answer.ToLower() == "remove")
                {
                    try
                    {
                        foreach (var item in ShechGarage.ListItems())
                        {
                            Console.WriteLine
                                ($" Car id {item.Id}\n\t Car name {item.CarName}\n\t Car color {item.Color}\n\t Car speed {item.Speed}\n\t Car graduation year {item.GraduationYear}");
                        }
                        Console.WriteLine("Enter car id");
                        string id = Console.ReadLine();
                        if (int.TryParse(id, out int Id)) { ShechGarage.Delete(Id); }
                    }
                    catch (ArgumentException ex)
                    {
                        Console.WriteLine(ex.Message);
                    }
                }
                else if (answer.ToLower() == "ride")
                {
                    Console.WriteLine("Find by wich parameter? (name/color/speed/year/all)");
                    answer = Console.ReadLine();
                    if (answer.ToLower() == "name")
                    {
                        try
                        {
                            Console.WriteLine("Enter car name");
                            string carName = Console.ReadLine();
                            foreach (var item in ShechGarage.FindItems(carName))
                            {
                                Console.WriteLine
                                   ($" Car id {item.Id}\n\t Car name {item.CarName}\n\t Car color {item.Color}\n\t Car speed {item.Speed}\n\t Car graduation year {item.GraduationYear}");
                            }
                            Console.WriteLine("Enter id of car on wich you want to ride");
                            answer = Console.ReadLine();
                            if (int.TryParse(answer, out int Id)) { Console.WriteLine(ShechGarage.Ride(Id)); }
                            else{ Console.WriteLine("Incorrect answer"); }
                        }
                        catch (ArgumentException ex)
                        {
                            Console.WriteLine(ex.Message);
                        }
                    }
                    else if (answer.ToLower() == "color")
                    {
                        try
                        {
                            Console.WriteLine("Enter car color");
                            string carColor = Console.ReadLine();
                            foreach (var item in ShechGarage.FindItems(carColor.ToCharArray()))
                            {
                                Console.WriteLine
                                   ($" Car id {item.Id}\n\t Car name {item.CarName}\n\t Car color {item.Color}\n\t Car speed {item.Speed}\n\t Car graduation year {item.GraduationYear}");
                            }
                            Console.WriteLine("Enter id of car on wich you want to ride");
                            answer = Console.ReadLine();
                            if (int.TryParse(answer, out int Id)) { Console.WriteLine(ShechGarage.Ride(Id)); }
                            else { Console.WriteLine("Incorrect answer"); }
                        }
                        catch (ArgumentException ex)
                        {
                            Console.WriteLine(ex.Message);
                        }
                    }
                    else if (answer.ToLower() == "speed")
                    {
                        try
                        {
                            Console.WriteLine("Enter car speed");
                            string carSpeed = Console.ReadLine();
                            if (int.TryParse(carSpeed, out int Speed))
                            {
                                foreach (var item in ShechGarage.FindItems(Speed))
                                {
                                    Console.WriteLine
                                       ($" Car id {item.Id}\n\t Car name {item.CarName}\n\t Car color {item.Color}\n\t Car speed {item.Speed}\n\t Car graduation year {item.GraduationYear}");
                                }
                                Console.WriteLine("Enter id of car on wich you want to ride");
                                answer = Console.ReadLine();
                                if (int.TryParse(answer, out int Id)) { Console.WriteLine(ShechGarage.Ride(Id)); }
                                else { Console.WriteLine("Incorrect answer"); }
                            }
                        }
                        catch (ArgumentException ex)
                        {
                            Console.WriteLine(ex.Message);
                        }
                    }
                    else if (answer.ToLower() == "year")
                    {
                        try
                        {
                            Console.WriteLine("Enter car gaduation year");
                            string year = Console.ReadLine();
                            if (float.TryParse(year, out float Year))
                            {
                                foreach (var item in ShechGarage.FindItems(Year))
                                {
                                    Console.WriteLine
                                       ($" Car id {item.Id}\n\t Car name {item.CarName}\n\t Car color {item.Color}\n\t Car speed {item.Speed}\n\t Car graduation year {item.GraduationYear}");
                                }
                            }
                            Console.WriteLine("Enter id of car on wich you want to ride");
                            answer = Console.ReadLine();
                            if (int.TryParse(answer, out int Id)) { Console.WriteLine(ShechGarage.Ride(Id)); }
                            else { Console.WriteLine("Incorrect answer"); }
                        }
                        catch (ArgumentException ex)
                        {
                            Console.WriteLine(ex.Message);
                        }
                    }
                    else if (answer.ToLower() == "all")
                    {
                        Console.WriteLine("Enter car name");
                        string carName = Console.ReadLine();
                        Console.WriteLine("Enter car color");
                        string carColor = Console.ReadLine();
                        Console.WriteLine("Enter car speed");
                        string carSpeed = Console.ReadLine();
                        Console.WriteLine("Enter car gaduation year");
                        string year = Console.ReadLine();

                        if (int.TryParse(carSpeed, out int Speed) && int.TryParse(year, out int Year))
                        {
                            try
                            {
                                foreach (var item in ShechGarage.FindItems(carName, carColor, Speed, Year))
                                {
                                    Console.WriteLine
                                       ($" Car id {item.Id}\n\t Car name {item.CarName}\n\t Car color {item.Color}\n\t Car speed {item.Speed}\n\t Car graduation year {item.GraduationYear}");
                                }
                                Console.WriteLine("Enter id of car on wich you want to ride");
                                answer = Console.ReadLine();
                                if (int.TryParse(answer, out int Id)) { Console.WriteLine(ShechGarage.Ride(Id)); }
                                else { Console.WriteLine("Incorrect answer"); }
                            }
                            catch (ArgumentException ex)
                            {
                                Console.WriteLine(ex.Message);
                            }
                        }
                    }
                }
            }
        }
    }
}

