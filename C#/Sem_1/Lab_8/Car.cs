﻿using System;

namespace Lab_8
{
    public class Car
    {
        public int Id { get; set; }
        private string carName;
        public string CarName
        {
            get
            {
                return carName;
            }
            set
            {
                if (value.Length <= 0)
                {
                    throw new ArgumentException("Car name length cannt be less than 0");
                }
                carName = value;
            }
        }
        private string color;

        public string Color {
            get
            {
                return color;
            }
            set
            {
                if (value.Length <= 0)
                {
                    throw new ArgumentException("Car color length cannt be less than 0");
                }
                color = value;
            }
        }
        private int speed;
        public int Speed
        {
            get 
            {
                return speed;
            }
            set 
            {
                if (value < 0)
                {
                    throw new ArgumentException("Speed cannt be less than 0");
                }
                speed = value;
            } 
        }
        private int graduationYear;

        public int GraduationYear
        {
            get
            {
                return graduationYear;
            }
            set
            {
                if (value < 0)
                {
                    throw new ArgumentException("Year cannt be less than 0");
                }
                graduationYear = value;
            }
        }

    }
}
