﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Lab_7
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }


        private string endPath = "C:\\Test\\Folder_0\\Folder_1\\Folder_2\\Folder_3\\Folder_4\\Folder_5\\Folder_6\\Folder_7\\Folder_8\\Folder_9\\Folder_10\\Folder_11\\Folder_12\\Folder_13\\Folder_14\\Folder_15\\Folder_16\\Folder_17\\Folder_18\\Folder_19\\Folder_20\\Folder_21\\Folder_22\\Folder_23\\Folder_24\\Folder_25\\Folder_26\\Folder_27\\Folder_28\\Folder_29\\Folder_30\\Folder_31\\Folder_32\\Folder_33\\Folder_34\\Folder_35\\Folder_36\\Folder_37\\Folder_38\\Folder_39\\Folder_40\\Folder_41\\Folder_42\\Folder_43\\Folder_44\\Folder_45\\Folder_46\\Folder_47\\Folder_48\\Folder_49\\Folder_50\\Folder_51\\Folder_52\\Folder_53\\Folder_54\\Folder_55\\Folder_56\\Folder_57\\Folder_58\\Folder_59\\Folder_60\\Folder_61\\Folder_62\\Folder_63\\Folder_64\\Folder_65\\Folder_66\\Folder_67\\Folder_68\\Folder_69\\Folder_70\\Folder_71\\Folder_72\\Folder_73\\Folder_74\\Folder_75\\Folder_76\\Folder_77\\Folder_78\\Folder_79\\Folder_80\\Folder_81\\Folder_82\\Folder_83\\Folder_84\\Folder_85\\Folder_86\\Folder_87\\Folder_88\\Folder_89\\Folder_90\\Folder_91\\Folder_92\\Folder_93\\Folder_94\\Folder_95\\Folder_96\\Folder_97\\Folder_98\\Folder_99\\Folder_100";

        private void CreateRemoveFolder(string directory, bool createMode)
        {
            if (createMode)
            {
                if (!Directory.Exists(directory))
                {
                    Directory.CreateDirectory(directory);
                }
            }
            else
            {
                if (Directory.Exists(directory))
                {
                    Directory.Delete(directory);
                }
            }
        }

        private void Remove_Click(object sender, RoutedEventArgs e)
        {
            for (int i = 0; i <= 100; i++)
            {
                CreateRemoveFolder(@"C:\Test\Folder_" + i, false);
            }

            Directory.Delete(@"C:\Test");
        }

        private void Create_Click(object sender, RoutedEventArgs e)
        {
            for (int i = 0; i <= 100; i++)
            {
                CreateRemoveFolder(@"C:\Test\Folder_" + i, true);
            }
        }

        private void RemoveInner_Click(object sender, RoutedEventArgs e)
        {
            CreateRemoveFolder(endPath, false);

            for (int i = 100; i > 0; i--)
            {
                string[] path = endPath.Split(new[] { "\\Folder_" + i }, StringSplitOptions.None);
                CreateRemoveFolder(path[0], false);
            }
            CreateRemoveFolder("C:\\Test", false);
        }

        private void CreateInner_Click(object sender, RoutedEventArgs e)
        {
            var oldName = "0\\";
            for (int i = 1; i <= 100; i++)
            {
                CreateRemoveFolder(@"C:\Test\Folder_" + oldName + "\\" + "Folder_" + i, true);
                oldName = oldName + "Folder_" + i + "\\";
            }
        }

        private void FindMaxInnerFolderName_Click(object sender, RoutedEventArgs e)
        {
            string[] info = new string[1];

            try
            {
                info = Directory.GetDirectories(path.Text);
            }
            catch (Exception)
            {
                path.Text = "Incorrect path";
                return;
            }

            while (info.Length > 0)
            {
                if (Directory.GetDirectories(info[0]).Length == 0)
                {
                    break;
                }
                info = Directory.GetDirectories(info[0]);
            }

            path.Text = info[0];
        }

        private void FindFile_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var file = fileName.Text;
                var folderPath = folder.Text;
                string[] allFoundFiles = Directory.GetFiles(folderPath, file, SearchOption.AllDirectories);
                fileName.Text = allFoundFiles[0];
            }
            catch (Exception)
            {
                fileName.Text = "Incorrect input";
            }
           
        }

        private void ReadFile_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var filePath = readFilePath.Text;
                byte[] data = File.ReadAllBytes(filePath);
                string str = Encoding.ASCII.GetString(data);
                text.Text = str;
            }
            catch (Exception)
            {
                text.Text = "Incorrect input";
            }
           
        }

    }
}
