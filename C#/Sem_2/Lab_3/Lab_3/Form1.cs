﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using Timer = System.Windows.Forms.Timer;

namespace Lab_3
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            Text = "Press button 'OK'";
        }

        private Point _point;

        private void Header()
        {
            Timer timer = new Timer();
            timer.Tick += timer_Tick;
            timer.Interval = 500;
            timer.Start();
        }

        // The timer's Tick event.
        private bool BlinkOn = false;
        private int BlinkCount;
        private int MoveCount;

        private void timer_Tick(object sender, EventArgs e)
        {
            if (BlinkOn || BlinkCount >= 16)
            {
                Text = "Press button 'OK'";
                BlinkOn = !BlinkOn;
                BlinkCount++;
            }
            else
            {
                Text = "";
                BlinkOn = !BlinkOn;
                BlinkCount++;
            }
            if (MoveCount >= 10)
            {
                Text = "Button 'OK', can't be pressed";
            }
        }

        private void Form1_MouseEnter(object sender, MouseEventArgs e)
        {
            _point = new Point(e.X, e.Y);
        }

        private void area_MouseEnter(object sender, EventArgs e)
        {
            var rnd = new Random();
            area.Location = new Point(rnd.Next(this.Width - area.Width), rnd.Next(this.Height - area.Height));
            ok.Location = new Point(area.Location.X + 50, area.Location.Y + 25);
            ok.Size = new Size(ok.Width, ok.Height - 3);
            MoveCount++;
        }

        private void Form1_Shown(object sender, EventArgs e)
        {
           Header();
        }

        private void ok_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Button 'OK' was pressed");
        }

        private void cancel_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
