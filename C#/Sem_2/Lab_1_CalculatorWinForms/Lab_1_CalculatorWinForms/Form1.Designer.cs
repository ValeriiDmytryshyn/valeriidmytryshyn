﻿
namespace Lab_1_CalculatorWinForms
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.number = new System.Windows.Forms.TextBox();
            this.results = new System.Windows.Forms.ListBox();
            this.one = new System.Windows.Forms.Button();
            this.two = new System.Windows.Forms.Button();
            this.three = new System.Windows.Forms.Button();
            this.four = new System.Windows.Forms.Button();
            this.five = new System.Windows.Forms.Button();
            this.six = new System.Windows.Forms.Button();
            this.seven = new System.Windows.Forms.Button();
            this.eight = new System.Windows.Forms.Button();
            this.nine = new System.Windows.Forms.Button();
            this.decipoint = new System.Windows.Forms.Button();
            this.zero = new System.Windows.Forms.Button();
            this.enter = new System.Windows.Forms.Button();
            this.clearentry = new System.Windows.Forms.Button();
            this.sign = new System.Windows.Forms.Button();
            this.plus = new System.Windows.Forms.Button();
            this.minus = new System.Windows.Forms.Button();
            this.multiple = new System.Windows.Forms.Button();
            this.divide = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // number
            // 
            this.number.Location = new System.Drawing.Point(36, 12);
            this.number.Name = "number";
            this.number.Size = new System.Drawing.Size(319, 23);
            this.number.TabIndex = 0;
            // 
            // results
            // 
            this.results.FormattingEnabled = true;
            this.results.ItemHeight = 15;
            this.results.Location = new System.Drawing.Point(36, 46);
            this.results.Name = "results";
            this.results.Size = new System.Drawing.Size(119, 184);
            this.results.TabIndex = 1;
            // 
            // one
            // 
            this.one.Location = new System.Drawing.Point(161, 41);
            this.one.Name = "one";
            this.one.Size = new System.Drawing.Size(42, 33);
            this.one.TabIndex = 2;
            this.one.Text = "1";
            this.one.UseVisualStyleBackColor = true;
            this.one.Click += new System.EventHandler(this.one_Click);
            // 
            // two
            // 
            this.two.Location = new System.Drawing.Point(209, 41);
            this.two.Name = "two";
            this.two.Size = new System.Drawing.Size(42, 33);
            this.two.TabIndex = 3;
            this.two.Text = "2";
            this.two.UseVisualStyleBackColor = true;
            this.two.Click += new System.EventHandler(this.two_Click);
            // 
            // three
            // 
            this.three.Location = new System.Drawing.Point(257, 41);
            this.three.Name = "three";
            this.three.Size = new System.Drawing.Size(42, 33);
            this.three.TabIndex = 4;
            this.three.Text = "3";
            this.three.UseVisualStyleBackColor = true;
            this.three.Click += new System.EventHandler(this.three_Click);
            // 
            // four
            // 
            this.four.Location = new System.Drawing.Point(161, 80);
            this.four.Name = "four";
            this.four.Size = new System.Drawing.Size(42, 33);
            this.four.TabIndex = 5;
            this.four.Text = "4";
            this.four.UseVisualStyleBackColor = true;
            this.four.Click += new System.EventHandler(this.four_Click);
            // 
            // five
            // 
            this.five.Location = new System.Drawing.Point(209, 80);
            this.five.Name = "five";
            this.five.Size = new System.Drawing.Size(42, 33);
            this.five.TabIndex = 6;
            this.five.Text = "5";
            this.five.UseVisualStyleBackColor = true;
            this.five.Click += new System.EventHandler(this.five_Click);
            // 
            // six
            // 
            this.six.Location = new System.Drawing.Point(257, 80);
            this.six.Name = "six";
            this.six.Size = new System.Drawing.Size(42, 33);
            this.six.TabIndex = 7;
            this.six.Text = "6";
            this.six.UseVisualStyleBackColor = true;
            this.six.Click += new System.EventHandler(this.six_Click);
            // 
            // seven
            // 
            this.seven.Location = new System.Drawing.Point(161, 119);
            this.seven.Name = "seven";
            this.seven.Size = new System.Drawing.Size(42, 33);
            this.seven.TabIndex = 8;
            this.seven.Text = "7";
            this.seven.UseVisualStyleBackColor = true;
            this.seven.Click += new System.EventHandler(this.seven_Click);
            // 
            // eight
            // 
            this.eight.Location = new System.Drawing.Point(209, 119);
            this.eight.Name = "eight";
            this.eight.Size = new System.Drawing.Size(42, 33);
            this.eight.TabIndex = 9;
            this.eight.Text = "8";
            this.eight.UseVisualStyleBackColor = true;
            this.eight.Click += new System.EventHandler(this.eight_Click);
            // 
            // nine
            // 
            this.nine.Location = new System.Drawing.Point(257, 119);
            this.nine.Name = "nine";
            this.nine.Size = new System.Drawing.Size(42, 33);
            this.nine.TabIndex = 10;
            this.nine.Text = "9";
            this.nine.UseVisualStyleBackColor = true;
            this.nine.Click += new System.EventHandler(this.nine_Click);
            // 
            // decipoint
            // 
            this.decipoint.Location = new System.Drawing.Point(161, 158);
            this.decipoint.Name = "decipoint";
            this.decipoint.Size = new System.Drawing.Size(42, 33);
            this.decipoint.TabIndex = 11;
            this.decipoint.Text = ".";
            this.decipoint.UseVisualStyleBackColor = true;
            this.decipoint.Click += new System.EventHandler(this.decipoint_Click);
            // 
            // zero
            // 
            this.zero.Location = new System.Drawing.Point(209, 158);
            this.zero.Name = "zero";
            this.zero.Size = new System.Drawing.Size(42, 33);
            this.zero.TabIndex = 12;
            this.zero.Text = "0";
            this.zero.UseVisualStyleBackColor = true;
            this.zero.Click += new System.EventHandler(this.zero_Click);
            // 
            // enter
            // 
            this.enter.Location = new System.Drawing.Point(257, 158);
            this.enter.Name = "enter";
            this.enter.Size = new System.Drawing.Size(98, 33);
            this.enter.TabIndex = 13;
            this.enter.Text = "Enter";
            this.enter.UseVisualStyleBackColor = true;
            this.enter.Click += new System.EventHandler(this.enter_Click);
            // 
            // clearentry
            // 
            this.clearentry.Location = new System.Drawing.Point(305, 80);
            this.clearentry.Name = "clearentry";
            this.clearentry.Size = new System.Drawing.Size(50, 72);
            this.clearentry.TabIndex = 14;
            this.clearentry.Text = "C";
            this.clearentry.UseVisualStyleBackColor = true;
            this.clearentry.Click += new System.EventHandler(this.clear_Click);
            // 
            // sign
            // 
            this.sign.Location = new System.Drawing.Point(305, 41);
            this.sign.Name = "sign";
            this.sign.Size = new System.Drawing.Size(50, 33);
            this.sign.TabIndex = 15;
            this.sign.Text = "+/-";
            this.sign.UseVisualStyleBackColor = true;
            this.sign.Click += new System.EventHandler(this.sign_Click);
            // 
            // plus
            // 
            this.plus.Location = new System.Drawing.Point(161, 197);
            this.plus.Name = "plus";
            this.plus.Size = new System.Drawing.Size(42, 33);
            this.plus.TabIndex = 16;
            this.plus.Text = "+";
            this.plus.UseVisualStyleBackColor = true;
            this.plus.Click += new System.EventHandler(this.add_Click);
            // 
            // minus
            // 
            this.minus.Location = new System.Drawing.Point(209, 197);
            this.minus.Name = "minus";
            this.minus.Size = new System.Drawing.Size(42, 33);
            this.minus.TabIndex = 17;
            this.minus.Text = "-";
            this.minus.UseVisualStyleBackColor = true;
            this.minus.Click += new System.EventHandler(this.minus_Click);
            // 
            // multiple
            // 
            this.multiple.Location = new System.Drawing.Point(257, 197);
            this.multiple.Name = "multiple";
            this.multiple.Size = new System.Drawing.Size(42, 33);
            this.multiple.TabIndex = 18;
            this.multiple.Text = "x";
            this.multiple.UseVisualStyleBackColor = true;
            this.multiple.Click += new System.EventHandler(this.multiple_Click);
            // 
            // divide
            // 
            this.divide.Location = new System.Drawing.Point(305, 197);
            this.divide.Name = "divide";
            this.divide.Size = new System.Drawing.Size(50, 33);
            this.divide.TabIndex = 19;
            this.divide.Text = "/";
            this.divide.UseVisualStyleBackColor = true;
            this.divide.Click += new System.EventHandler(this.division_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(384, 238);
            this.Controls.Add(this.divide);
            this.Controls.Add(this.multiple);
            this.Controls.Add(this.minus);
            this.Controls.Add(this.plus);
            this.Controls.Add(this.sign);
            this.Controls.Add(this.clearentry);
            this.Controls.Add(this.enter);
            this.Controls.Add(this.zero);
            this.Controls.Add(this.decipoint);
            this.Controls.Add(this.nine);
            this.Controls.Add(this.eight);
            this.Controls.Add(this.seven);
            this.Controls.Add(this.six);
            this.Controls.Add(this.five);
            this.Controls.Add(this.four);
            this.Controls.Add(this.three);
            this.Controls.Add(this.two);
            this.Controls.Add(this.one);
            this.Controls.Add(this.results);
            this.Controls.Add(this.number);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox number;
        private System.Windows.Forms.ListBox results;
        private System.Windows.Forms.Button one;
        private System.Windows.Forms.Button two;
        private System.Windows.Forms.Button three;
        private System.Windows.Forms.Button four;
        private System.Windows.Forms.Button five;
        private System.Windows.Forms.Button six;
        private System.Windows.Forms.Button seven;
        private System.Windows.Forms.Button eight;
        private System.Windows.Forms.Button nine;
        private System.Windows.Forms.Button decipoint;
        private System.Windows.Forms.Button zero;
        private System.Windows.Forms.Button enter;
        private System.Windows.Forms.Button clearentry;
        private System.Windows.Forms.Button sign;
        private System.Windows.Forms.Button plus;
        private System.Windows.Forms.Button minus;
        private System.Windows.Forms.Button multiple;
        private System.Windows.Forms.Button divide;
    }
}

