﻿using System;

namespace Lab_1_CalculatorWinForms
{
    class Operations
    {
        public enum Operators
        {
            None,
            Addition,
            Subtraction,
            Multiplication,
            Division
        };

        private Operators op;
        private Boolean firstOperation;
        private double answer;

        public Operations()
        {
            answer = 0.0;
            op = Operators.None;
            firstOperation = true;
        }

        public Boolean IsFirstOperation()
        {
            return firstOperation;
        }

        public String Operate(double a, double b)
        {
            firstOperation = false;

            switch (op)
            {
                case Operators.Addition:
                    answer = Addition(a, b);
                    break;
                case Operators.Subtraction:
                    answer = Subtraction(a, b);
                    break;
                case Operators.Multiplication:
                    answer = Multiplication(a, b);
                    break;
                case Operators.Division:
                    if (b == 0 || a == 0)
                    {
                        return "NaN";
                    }
                    else
                    {
                        answer = Division(a, b);
                    }
                    break;
            };

            return Convert.ToString(answer);
        }

        public String Operate(double a)
        {
            return Operate(answer, a);
        }

        public void SetOperation(Operators op)
        {
            this.op = op;
        }

        public void Reset()
        {
            firstOperation = true;
            op = Operators.None;
        }

        // Static operators
        static public double Addition(double a, double b)
        {
            return (a + b);
        }

        static public double Subtraction(double a, double b)
        {
            return (a - b);
        }

        static public double Multiplication(double a, double b)
        {
            return (a * b);
        }

        static public double Division(double a, double b)
        {
            return (a / b);
        }


        public Operators GetOperation()
        {
            return op;
        }
    }
}