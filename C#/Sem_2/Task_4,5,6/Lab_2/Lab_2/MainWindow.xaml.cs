﻿using System.Windows;

namespace Lab_2
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private int[] UserSequence = new int [3];

        private void add_Click(object sender, RoutedEventArgs e)
        {
            if (input.Text.Length > 0)
            {
                list.Items.Add(input.Text);
                list.SelectedIndex = list.Items.Count - 1;
                input.Text = "";
            }
        }

        private void remove_Click(object sender, RoutedEventArgs e)
        {
            if (list.Items.Count > 0)
            {
                list.Items.RemoveAt(list.Items.Count - 1);
            }
        }

        private void ResetSequence()
        {
            one.Visibility = Visibility.Visible;
            two.Visibility = Visibility.Visible;
            three.Visibility = Visibility.Visible;
            UserSequence = new int[3];
        }

        private void one_Click(object sender, RoutedEventArgs e)
        {
            if (UserSequence[0] != 0)
            {
                ResetSequence();
            }
            else
            {
                congratulations.Text = "";
                one.Visibility = Visibility.Hidden;
                UserSequence[0] = 1;
            }
        }

        private void two_Click(object sender, RoutedEventArgs e)
        {
            if (UserSequence[0] == 1)
            {
                two.Visibility = Visibility.Hidden;
                UserSequence[1] = 2;
            }
            else
            {
                ResetSequence();
            }
        }

        private void three_Click(object sender, RoutedEventArgs e)
        {
            if (UserSequence[1] == 2)
            {
                congratulations.Text = "Congratulations!!!";
            }

            ResetSequence();
        }
    }
}
