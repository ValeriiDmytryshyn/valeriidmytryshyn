﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace Task_3_WPF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            Title = "Press button 'OK'";
            Header();

        }
        private Point _point;

        private void Header()
        {
            DispatcherTimer timer = new DispatcherTimer();
            timer.Tick += timer_Tick;
            timer.Interval = new TimeSpan(0, 0, 0, 0, 500);
            timer.Start();
        }

        // The timer's Tick event.
        private bool BlinkOn = false;
        private int BlinkCount;
        private int MoveCount;

        private void timer_Tick(object sender, EventArgs e)
        {
            if (BlinkOn || BlinkCount >= 16)
            {
                Title = "Press button 'OK'";
                BlinkOn = !BlinkOn;
                BlinkCount++;
            }
            else
            {
                Title = "";
                BlinkOn = !BlinkOn;
                BlinkCount++;
            }
            if (MoveCount >= 10)
            {
                Title = "Button 'OK', can't be pressed";
            }
        }

        private void Grid_MouseMove(object sender, MouseEventArgs e)
        {
            _point = e.GetPosition(this);
        }

        private void area_MouseMove_1(object sender, MouseEventArgs e)
        {
            var rnd = new Random();
            area.Margin = new Thickness(rnd.Next(0, int.Parse((this.Width - area.Width).ToString())),
                rnd.Next(0, int.Parse((this.Height - area.Height).ToString())), ok.Margin.Right, ok.Margin.Bottom);

            ok.Margin = new Thickness(area.Margin.Left + 50, area.Margin.Top + 25, area.Margin.Right, area.Margin.Bottom);

            if (ok.Height >= 3)
            { 
                ok.Height = ok.Height - 3;
            }
            MoveCount++;
        }

        private void ok_Click_1(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("Button 'OK' was pressed");

        }

        private void cancel_Click_1(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
