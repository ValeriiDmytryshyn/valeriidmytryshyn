﻿using System;
using System.Windows;
using System.Windows.Controls;

namespace ValeriiDmytryshyn.Calculator
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        static string defaultString = "0.0";
        string numberOneBuffer;
        string numberTwoBuffer;
        readonly Operations c;
        public MainWindow()
        {
            InitializeComponent();
            c = new Operations();
            number.Text = defaultString;

            numberOneBuffer = "";
            numberTwoBuffer = "";
        }

        private void one_Click(object sender, RoutedEventArgs e)
        {
            numberOneBuffer += "1";
            number.Text = numberOneBuffer;
        }

        private void two_Click(object sender, RoutedEventArgs e)
        {
            numberOneBuffer += "2";
            number.Text = numberOneBuffer;
        }

        private void three_Click(object sender, RoutedEventArgs e)
        {
            numberOneBuffer += "3";
            number.Text = numberOneBuffer;
        }

        private void four_Click(object sender, RoutedEventArgs e)
        {
            numberOneBuffer += "4";
            number.Text = numberOneBuffer;
        }

        private void five_Click(object sender, RoutedEventArgs e)
        {
            numberOneBuffer += "5";
            number.Text = numberOneBuffer;
        }

        private void six_Click(object sender, RoutedEventArgs e)
        {
            numberOneBuffer += "6";
            number.Text = numberOneBuffer;
        }

        private void seven_Click(object sender, RoutedEventArgs e)
        {
            numberOneBuffer += "7";
            number.Text = numberOneBuffer;
        }

        private void eight_Click(object sender, RoutedEventArgs e)
        {
            numberOneBuffer += "8";
            number.Text = numberOneBuffer;
        }

        private void nine_Click(object sender, RoutedEventArgs e)
        {
            numberOneBuffer += "9";
            number.Text = numberOneBuffer;
        }

        private void zero_Click(object sender, RoutedEventArgs e)
        {
            numberOneBuffer += "0";
            number.Text = numberOneBuffer;
        }

        private void decipoint_Click(object sender, RoutedEventArgs e)
        {
            numberOneBuffer += ".";
        }

        private void enter_Click(object sender, RoutedEventArgs e)
        {
            string answer = "";
            if (numberTwoBuffer.Length <= 0)
            {
                numberTwoBuffer = "0";
            }
            else if(numberOneBuffer.Length <= 0)
            {
                numberOneBuffer = "0";
            }
            if (c.IsFirstOperation())
            {
                answer = c.Operate(Convert.ToDouble(numberTwoBuffer), Convert.ToDouble(numberOneBuffer));
            }
            else
            {
                answer = c.Operate(Convert.ToDouble(numberTwoBuffer));
            }

            number.Text = answer;
            updateList(answer);
            numberOneBuffer = "";
            numberTwoBuffer = "";
        }

        private void updateList(string answer)
        {
            string s = numberTwoBuffer;

            Operations.Operators op = c.GetOperation();
            switch (op)
            {
                case Operations.Operators.Addition:
                    s += " + ";
                    break;
                case Operations.Operators.Subtraction:
                    s += " - ";
                    break;
                case Operations.Operators.Multiplication:
                    s += " x ";
                    break;
                case Operations.Operators.Division:
                    s += " % ";
                    break;
            }

            s += numberOneBuffer;
            s += " = ";
            s += answer;
            results.Items.Add(s);
        }

        private void sign_Click(object sender, RoutedEventArgs e)
        {
            if (numberOneBuffer.Length > 0)
            {
                if (numberOneBuffer[0] == '-')
                {
                    numberOneBuffer = numberOneBuffer.Substring(1, numberOneBuffer.Length - 1);
                }
                else
                {
                    numberOneBuffer = "-" + numberOneBuffer;
                }
            }
            else
            {
                numberOneBuffer = "-" + numberOneBuffer;
            }
            number.Text = Convert.ToString(numberOneBuffer);
        }

        private void clear_Click(object sender, RoutedEventArgs e)
        {
            numberOneBuffer = "";
            c.Reset();
            number.Text = defaultString;
        }

        private void add_Click(object sender, RoutedEventArgs e)
        {
            if ((numberOneBuffer.Length > 0) && (numberTwoBuffer.Length > 0))
            {
                string answer = c.Operate(Convert.ToDouble(numberTwoBuffer), Convert.ToDouble(numberOneBuffer));
                number.Text = answer;
                c.SetOperation(Operations.Operators.Addition);
            }
            else
            {
                c.SetOperation(Operations.Operators.Addition);
                numberTwoBuffer = numberOneBuffer;
                numberOneBuffer = string.Empty;
            }

        }

        private void minus_Click(object sender, RoutedEventArgs e)
        {
            if ((numberOneBuffer.Length > 0) && (numberTwoBuffer.Length > 0))
            {
                number.Text = c.Operate(Convert.ToDouble(numberTwoBuffer), Convert.ToDouble(numberOneBuffer));
            }
            c.SetOperation(Operations.Operators.Subtraction);
            numberTwoBuffer = numberOneBuffer;
            numberOneBuffer = string.Empty;
        }

        private void multiple_Click(object sender, RoutedEventArgs e)
        {
            if ((numberOneBuffer.Length > 0) && (numberTwoBuffer.Length > 0))
            {
                number.Text = c.Operate(Convert.ToDouble(numberTwoBuffer), Convert.ToDouble(numberOneBuffer));
            }
            c.SetOperation(Operations.Operators.Multiplication);
            numberTwoBuffer = numberOneBuffer;
            numberOneBuffer = string.Empty;
        }

        private void division_Click(object sender, RoutedEventArgs e)
        {
            if ((numberOneBuffer.Length > 0) && (numberTwoBuffer.Length > 0))
            {
                number.Text = c.Operate(Convert.ToDouble(numberTwoBuffer), Convert.ToDouble(numberOneBuffer));
            }
            c.SetOperation(Operations.Operators.Division);
            numberTwoBuffer = numberOneBuffer;
            numberOneBuffer = string.Empty;
        }

        private void results_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }
    }
}