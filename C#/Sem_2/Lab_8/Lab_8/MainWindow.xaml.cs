﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Windows;

namespace Lab_8
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        List<string> LoadedLogs = new List<string>();
        List<(string, string)> Logs = new List<(string, string)>();
        public MainWindow()
        {
            InitializeComponent();
        }

        private void setProperties()
        {
            foreach (var item in LoadedLogs)
            {
                switch (item.Split(':')[0])
                {
                    case "textBox":
                        textBox.Text = item.Split(':')[1];
                        break;
                    case "checkBox1":
                        checkBox1.IsChecked = bool.Parse(item.Split(':')[1]);
                        break;
                    case "checkBox2":
                        checkBox2.IsChecked = bool.Parse(item.Split(':')[1]);
                        break;
                    case "Height":
                        Height = Int32.Parse(item.Split(':')[1]);
                        break;
                    case "Width":
                        Width = Int32.Parse(item.Split(':')[1]);
                        break;
                }
            }
        }
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            using (FileStream fs = new FileStream(@"./logs.txt", FileMode.OpenOrCreate))
            {
                using (StreamReader sr = new StreamReader(fs))
                {
                    string str;

                    while ((str = sr.ReadLine()) != null)
                    {
                        LoadedLogs.Add(str);
                    }
                }
            }

            setProperties();
        }
        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            Logs.Add((textBox.Name, textBox.Text));
            Logs.Add((checkBox1.Name, checkBox1.IsChecked.ToString()));
            Logs.Add((checkBox2.Name, checkBox2.IsChecked.ToString()));

            Logs.Add(("Height", Height.ToString()));
            Logs.Add(("Width", Width.ToString()));

            using (FileStream fs = new FileStream(@"./logs.txt", FileMode.OpenOrCreate))
            {
                using (StreamWriter sw = new StreamWriter(fs))
                {
                    foreach (var item in Logs)
                    {
                        sw.WriteLine($"{item.Item1}: {item.Item2}");
                    }
                }
            }
        }
    }
}
