﻿namespace Lab_9
{
    public static class CodeText
    {
        private const string LetterSet = "АБВГҐДЕЄЖЗИІЇЙКЛМНОПРСТУФХЦЧШЩЬЮЯABCDEFGHIJKLMNOPQRSTUVWXYZ";

        private static string CodeOrEncode(string text, int key, bool isCoded)
        {
            var letters = LetterSet + LetterSet.ToLower();
            var lettersLength = letters.Length;
            var value = "";

            if (isCoded) { key *= -1; }

            foreach (var item in text)
            {
                var coded = item;
                var index = letters.IndexOf(coded);
                if (index < 0)
                {
                    value += coded.ToString();
                }
                else
                {
                    var codeIndex = (lettersLength + index + key) % lettersLength;
                    value += letters[codeIndex];
                }
            }

            return value;
        }
        public static string Encrypt(string plainMessage, int key)
        {
            return CodeOrEncode(plainMessage, key, false);
        }
        public static string Decrypt(string encryptedMessage, int key)
        {
            return CodeOrEncode(encryptedMessage, key, true);
        }
    }
}