﻿using System;
using System.IO;
using System.Text;
using System.Windows;

namespace Lab_9
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Open_Click(object sender, RoutedEventArgs e)
        {
            var fileDialog = new System.Windows.Forms.OpenFileDialog();
            fileDialog.DefaultExt = ".txt";
            fileDialog.Filter = "Text files (*.txt)|*.txt";
            var result = fileDialog.ShowDialog();

            switch (result)
            {
                case System.Windows.Forms.DialogResult.OK:
                    var file = fileDialog.FileName;
                    TxtFile.Content = file;

                    using (FileStream fs = new FileStream(fileDialog.FileName, FileMode.Open))
                    {
                        using (TextReader sr = new StreamReader(fs, Encoding.Default))
                        {
                            try
                            {
                                if (!encryptRadio.IsChecked.Value)
                                {
                                    textArea.Text = CodeText.Decrypt(sr.ReadToEnd(), Int32.Parse(slideValue.Text));
                                }
                                else
                                {
                                    textArea.Text = CodeText.Encrypt(sr.ReadToEnd(), Int32.Parse(slideValue.Text));
                                }
                            }
                            catch(Exception)
                            {
                                MessageBox.Show("Check input fields", "Error");
                            }
                        }
                    }
                    break;
                case System.Windows.Forms.DialogResult.Cancel:
                default:
                    TxtFile.Content = "";
                    break;
            }
        }

        private void Save_Click(object sender, RoutedEventArgs e)
        {
            var fileDialog = new System.Windows.Forms.OpenFileDialog();
            fileDialog.ValidateNames = false;
            fileDialog.CheckFileExists = false;
            fileDialog.CheckPathExists = true;
            fileDialog.FileName = "text.txt";

            var result = fileDialog.ShowDialog();
            try
            {
                switch (result)
                {
                    case System.Windows.Forms.DialogResult.OK:
                        var file = fileDialog.FileName;
                        TxtFile.Content = file;

                        var x = result;

                        using (TextWriter tw = File.CreateText(fileDialog.FileName))
                        {
                            tw.Write(textArea.Text);
                        }

                        break;
                    case System.Windows.Forms.DialogResult.Cancel:
                    default:
                        TxtFile.Content = "";
                        break;
                }

            }
            catch (Exception exception)
            {
                Console.WriteLine(exception);
                throw;
            }
        }

    }
}
