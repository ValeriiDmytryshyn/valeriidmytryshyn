﻿
namespace Lab_2_winforms
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.TaskOne = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.list = new System.Windows.Forms.ComboBox();
            this.remove = new System.Windows.Forms.Button();
            this.add = new System.Windows.Forms.Button();
            this.input = new System.Windows.Forms.TextBox();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.congratulations = new System.Windows.Forms.TextBox();
            this.two = new System.Windows.Forms.Button();
            this.three = new System.Windows.Forms.Button();
            this.one = new System.Windows.Forms.Button();
            this.TaskOne.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.SuspendLayout();
            // 
            // TaskOne
            // 
            this.TaskOne.Controls.Add(this.tabPage1);
            this.TaskOne.Controls.Add(this.tabPage2);
            this.TaskOne.Location = new System.Drawing.Point(12, 12);
            this.TaskOne.Name = "TaskOne";
            this.TaskOne.SelectedIndex = 0;
            this.TaskOne.Size = new System.Drawing.Size(403, 290);
            this.TaskOne.TabIndex = 0;
            this.TaskOne.Tag = "";
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.list);
            this.tabPage1.Controls.Add(this.remove);
            this.tabPage1.Controls.Add(this.add);
            this.tabPage1.Controls.Add(this.input);
            this.tabPage1.Location = new System.Drawing.Point(4, 24);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Size = new System.Drawing.Size(395, 262);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Task One";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // list
            // 
            this.list.FormattingEnabled = true;
            this.list.Location = new System.Drawing.Point(17, 60);
            this.list.Name = "list";
            this.list.Size = new System.Drawing.Size(121, 23);
            this.list.TabIndex = 3;
            // 
            // remove
            // 
            this.remove.Location = new System.Drawing.Point(259, 21);
            this.remove.Name = "remove";
            this.remove.Size = new System.Drawing.Size(85, 24);
            this.remove.TabIndex = 2;
            this.remove.Text = "Remove";
            this.remove.UseVisualStyleBackColor = true;
            this.remove.Click += new System.EventHandler(this.remove_Click);
            // 
            // add
            // 
            this.add.Location = new System.Drawing.Point(168, 21);
            this.add.Name = "add";
            this.add.Size = new System.Drawing.Size(85, 24);
            this.add.TabIndex = 1;
            this.add.Text = "Add";
            this.add.UseVisualStyleBackColor = true;
            this.add.Click += new System.EventHandler(this.add_Click);
            // 
            // input
            // 
            this.input.BackColor = System.Drawing.Color.OrangeRed;
            this.input.ForeColor = System.Drawing.SystemColors.InfoText;
            this.input.Location = new System.Drawing.Point(17, 21);
            this.input.Name = "input";
            this.input.Size = new System.Drawing.Size(121, 23);
            this.input.TabIndex = 0;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.congratulations);
            this.tabPage2.Controls.Add(this.two);
            this.tabPage2.Controls.Add(this.three);
            this.tabPage2.Controls.Add(this.one);
            this.tabPage2.Location = new System.Drawing.Point(4, 24);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Size = new System.Drawing.Size(395, 262);
            this.tabPage2.TabIndex = 0;
            this.tabPage2.Text = "Task Two";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // congratulations
            // 
            this.congratulations.BackColor = System.Drawing.Color.Lime;
            this.congratulations.Location = new System.Drawing.Point(131, 140);
            this.congratulations.Name = "congratulations";
            this.congratulations.Size = new System.Drawing.Size(128, 23);
            this.congratulations.TabIndex = 3;
            // 
            // two
            // 
            this.two.Location = new System.Drawing.Point(279, 41);
            this.two.Name = "two";
            this.two.Size = new System.Drawing.Size(65, 57);
            this.two.TabIndex = 2;
            this.two.Text = "2";
            this.two.UseVisualStyleBackColor = true;
            this.two.Click += new System.EventHandler(this.two_Click);
            // 
            // three
            // 
            this.three.Location = new System.Drawing.Point(171, 41);
            this.three.Name = "three";
            this.three.Size = new System.Drawing.Size(65, 57);
            this.three.TabIndex = 1;
            this.three.Text = "3";
            this.three.UseVisualStyleBackColor = true;
            this.three.Click += new System.EventHandler(this.three_Click);
            // 
            // one
            // 
            this.one.Location = new System.Drawing.Point(58, 41);
            this.one.Name = "one";
            this.one.Size = new System.Drawing.Size(65, 57);
            this.one.TabIndex = 0;
            this.one.Text = "1";
            this.one.UseVisualStyleBackColor = true;
            this.one.Click += new System.EventHandler(this.one_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.TaskOne);
            this.Name = "Form1";
            this.Text = "Form1";
            this.TaskOne.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl TaskOne;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Button remove;
        private System.Windows.Forms.Button add;
        private System.Windows.Forms.TextBox input;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.ComboBox list;
        private System.Windows.Forms.TextBox congratulations;
        private System.Windows.Forms.Button two;
        private System.Windows.Forms.Button three;
        private System.Windows.Forms.Button one;
    }
}

