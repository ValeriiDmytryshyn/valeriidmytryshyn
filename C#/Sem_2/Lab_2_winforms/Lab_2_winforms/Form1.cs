﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab_2_winforms
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        private int[] UserSequence = new int[3];

        private void add_Click(object sender, EventArgs e)
        {
            if (input.Text.Length > 0)
            {
                list.Items.Add(input.Text);
                list.SelectedIndex = list.Items.Count - 1;
                input.Text = "";
            }
        }

        private void remove_Click(object sender, EventArgs e)
        {
            if (list.Items.Count > 0)
            {
                list.Items.RemoveAt(list.Items.Count - 1);
            }
        }

        private void ResetSequence()
        {
            one.Show();
            two.Show();
            three.Show();
            UserSequence = new int[3];
        }

        private void one_Click(object sender, EventArgs e)
        {
            if (UserSequence[0] != 0)
            {
                ResetSequence();
            }
            else
            {
                congratulations.Text = "";
                one.Hide();
                UserSequence[0] = 1;
            }
        }

        private void two_Click(object sender, EventArgs e)
        {
            if (UserSequence[0] == 1)
            {
                two.Hide();
                UserSequence[1] = 2;
            }
            else
            {
                ResetSequence();
            }
        }

        private void three_Click(object sender, EventArgs e)
        {
            if (UserSequence[1] == 2)
            {
                congratulations.Text = "Congratulations!!!";
            }

            ResetSequence();
        }
    }
}
